package com.example.assignment2;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
private EditText username,password;
private Button signup,login;
String fname,lname,desh,email,pass,cpass,pnum,adress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView username=(TextView) findViewById(R.id.first);
        TextView password=(TextView) findViewById(R.id.second);
        Button signup=(Button) findViewById(R.id.fourth);
        Button login=(Button) findViewById(R.id.third);

















        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                fname= null;
                lname= null;
                desh= null;
                email= null;
                pass= null;
                cpass= null;
                pnum=null;

            } else {
               fname= extras.getString("fname1");
                lname= extras.getString("lname1");
                email= extras.getString("email1");
                desh= extras.getString("desh1");
                pass= extras.getString("pass1");
                cpass= extras.getString("cpass1");
                pnum= extras.getString("pnum1");
                adress= extras.getString("adress1");


            }
        } else {

            fname= (String) savedInstanceState.getSerializable("fname1");
            lname= (String) savedInstanceState.getSerializable("lname1");
            email= (String) savedInstanceState.getSerializable("emai1");
            desh= (String) savedInstanceState.getSerializable("desh1");
            pass= (String) savedInstanceState.getSerializable("pass1");
            cpass= (String) savedInstanceState.getSerializable("cpass1");
            pnum= (String) savedInstanceState.getSerializable("pnum1");
            adress= (String) savedInstanceState.getSerializable("adress1");

        }

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String username1 = username.getText().toString();
                String password1 = password.getText().toString();

               if (username1.equals(email))
                {
                  if (password1.equals(pass))
                   {
                      Toast.makeText(MainActivity.this,"login Successful",Toast.LENGTH_SHORT).show();
                       Intent intent=new Intent(MainActivity.this, homescreen.class);

                       intent.putExtra("fname1",fname);

                       intent.putExtra("lname1",lname);

                       intent.putExtra("email1",email);

                       intent.putExtra("desh1",desh);

                       intent.putExtra("pass1",pass);

                       intent.putExtra("cpass1",cpass);

                       intent.putExtra("pnum1",pnum);

                       intent.putExtra("adress1",adress);
                       startActivity(intent);
                   }
                  else
                  {

                      Toast.makeText(MainActivity.this,"Password wrong",Toast.LENGTH_SHORT).show();
                  }
                }
               else
               {

                   Toast.makeText(MainActivity.this,"email wrong",Toast.LENGTH_SHORT).show();
               }

            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, signup.class);
                startActivity(intent);
            }
        });


    }
}