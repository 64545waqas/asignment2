package com.example.assignment2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class homescreen extends AppCompatActivity {
    private TextView name,roll,batch,campus,degree;
    private Button exitbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);
        TextView name=(TextView) findViewById(R.id.name);
        TextView roll=(TextView) findViewById(R.id.roll);
        TextView batch=(TextView) findViewById(R.id.batch);
        TextView campus=(TextView) findViewById(R.id.campus);
        TextView degree=(TextView) findViewById(R.id.degree);
        Button exitbtn=(Button)  findViewById(R.id.exit);
        String name1=name.getText().toString();
        String roll1=roll.getText().toString();
        String batch1=batch.getText().toString();
        String campus1=campus.getText().toString();
        String degree1=degree.getText().toString();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            if(extras == null) {
                name1= null;
                roll1= null;
                batch1= null;
                campus1= null;
                degree1= null;


            } else {
                name1= extras.getString("fname1");
                roll1= extras.getString("desh1");
                batch1= extras.getString("email1");
                campus1= extras.getString("adress");
                degree1= extras.getString("pnum");


            }
        }
         name.setText(name1);
        roll.setText(roll1);
        batch.setText(batch1);
        campus.setText(campus1);
        degree.setText(degree1);



    }

}