package com.example.assignment2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class signup extends AppCompatActivity {
    private EditText fname,lname,email,desh,pass,cpass,pnum,adress;
    private Button signup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        TextView fname=(TextView) findViewById(R.id.first);
        TextView lname=(TextView) findViewById(R.id.lfirst);
        TextView email=(TextView) findViewById(R.id.second);
        TextView desh=(TextView) findViewById(R.id.third);
        TextView pass=(TextView) findViewById(R.id.fourth);
        TextView cpass=(TextView) findViewById(R.id.five);
        TextView pnum=(TextView) findViewById(R.id.six);
        TextView adress=(TextView) findViewById(R.id.seven);
        Button signup=(Button) findViewById(R.id.eight);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(signup.this, MainActivity.class);
                String fname1=fname.getText().toString();
                intent.putExtra("fname1",fname1);
                String lname1=lname.getText().toString();
                intent.putExtra("lname1",lname1);
                String email1=email.getText().toString();
                intent.putExtra("email1",email1);
                String desh1=desh.getText().toString();
                intent.putExtra("desh1",desh1);
                String pass1=pass.getText().toString();
                intent.putExtra("pass1",pass1);
                String cpass1=cpass.getText().toString();
                intent.putExtra("cpass1",cpass1);
                String pnum1=pnum.getText().toString();
                intent.putExtra("pnum1",pnum1);
                String adress1=adress.getText().toString();
                intent.putExtra("adress1",adress1);



                startActivity(intent);
            }
        });

    }
}